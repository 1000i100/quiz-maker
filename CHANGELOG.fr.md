[EN](CHANGELOG.md) | FR

# Changelog | liste des changements
Tous les changements notables de ce projet seront documenté dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnage sémantique](https://semver.org/lang/fr/).

## [Non-publié/Non-Stabilisé] (par [1000i100])
### Ajouté
- restructuration massive du projet pour :
  - publier quiz-maker sur npm en tant que packet statique sans dépendances
  - générer des métriques qualité pour visibiliser le travail invisible à court terme.
  - présenter le projet dans plusieurs langues (au moins anglais et français)

## [Version 0.1.1] - 2021-03-09 (par [1000i100])
### Ajouté
- option headerText pour ajouter un texte d'introduction au quiz
- ajustement de style

## [Version 0.1.0] - 2020-02-28 (par [1000i100])
### Ajouté
- Plusieurs exemples
- 2 modèles d'affichage des résultats (dont trikala sommaire)
- tout un tas d'options pour configurer ses quiz (cf exemples)
- probablement de la dette technique

## [Version 0.0.1] - 2019-12-06 (par [1000i100])
### Ajouté
- Prototype générateur web
- Prototype générateur cli
- Un permier exemple

[Non-publié/Non-Stabilisé]: https://framagit.org/1000i100/quiz-maker/-/compare/v0.1.1...main

[Version 0.1.1]: https://framagit.org/1000i100/quiz-maker/-/compare/v0.1.0...v0.1.1
[Version 0.1.0]: https://framagit.org/1000i100/quiz-maker/-/compare/v0.0.1...v0.1.0
[Version 0.0.1]: https://framagit.org/1000i100/quiz-maker/-/tree/v0.0.1

[1000i100]: https://framagit.org/1000i100 "@1000i100"
