import test from 'ava';
import * as app from '../generated/tmp/quizParser.mjs';
//const app = require('../generated/tmp/quizParser.cjs');

// avec titre
// avec texte d'intro pour le quizz
// avec texte sur l'écran de validation
// avec min et max de réponse par question
// avec randomization de l'ordre des questions
// avec randomization de l'ordre des réponses
// sans secret (avec quotation disponnible publiquement)
// avec barème de prise en compte de la quotation
// avec légende pour la génération de graphique en triangle.

// avec possibilité de gras/gros pour les questions et pour les réponses de quiz
// avec possibilité d'image pour les questions et réponses du quiz



const testCases = {
	'Q\n[A] a\n[B] b': {questions: [{label:"Q",answers:[{label:"a",quotation:"A"},{label:"b",quotation:"B"}]}]},
	'Q // comment\n[A] a\n[B] b\n': {questions: [{label:"Q",answers:[{label:"a",quotation:"A"},{label:"b",quotation:"B"}]}]},
	'Q\n[A] /* a block comment*/ a\n[B] b\n': {questions: [{label:"Q",answers:[{label:"a",quotation:"A"},{label:"b",quotation:"B"}]}]},
	'min: 1\n---\nQ\n[A] a\n[B] b\n': {options: {min:1}, questions: [{label:"Q",answers:[{label:"a",quotation:"A"},{label:"b",quotation:"B"}]}]},
};
Object.keys(testCases).forEach((k)=>test(k,t=>t.deepEqual(app.parse(k),testCases[k])));
