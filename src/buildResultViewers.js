const fs = require("fs");
const ejs = require('ejs');

let str = `const resultViewers = {};
`;
function splitNameExt(fileName){
	const pathParts = fileName.split('/');
	const fileNameParts = pathParts.pop().split('.');
	let path = pathParts.join('/');
	if(path) path+='/';
	let ext = '.'+fileNameParts.pop();
	let name = fileNameParts.join('.');
	return {path,name,ext}
}
fs.readdirSync('./src/resultViewer').forEach((fileName)=>{
	const filePath = `./src/resultViewer/${fileName}`;
	let f = splitNameExt(filePath);
	if(f.ext==='.html') return;
  if(f.ext ==='.mjs') f.path = '../../src/resultViewer/';
  if(f.ext==='.ejs'){
		f = splitNameExt(f.path+f.name);
		f.path = './rv.';
		fs.writeFileSync(`./generated/tmp/${f.path+f.name+f.ext}`, ejs.render(fs.readFileSync(filePath, 'utf8'),{},{filename:filePath}));
	}
	str+=`
import ${f.name==='default'?'defaultView':f.name} from "${f.path+f.name+f.ext}";
resultViewers['${f.name}'] = ${f.name==='default'?'defaultView':f.name};
`;
});
str+=`
export default resultViewers;
`;

fs.writeFileSync(`./generated/tmp/importAll.mjs`, str);
