[![pipeline status](https://framagit.org/1000i100/quiz-maker/badges/main/pipeline.svg)](https://framagit.org/1000i100/quiz-maker/-/pipelines)
[![coverage report](https://framagit.org/1000i100/quiz-maker/badges/main/coverage.svg)](https://1000i100.frama.io/quiz-maker/coverage/)

[![duplication](https://1000i100.frama.io/quiz-maker/jscpd-badge.svg)](https://1000i100.frama.io/quiz-maker/jscpd/)
[![maintainability](https://1000i100.frama.io/quiz-maker/worst-maintainability.svg)](https://1000i100.frama.io/quiz-maker/maintainability/)

[![release](https://img.shields.io/npm/v/quiz-maker.svg)](https://www.npmjs.com/package/quiz-maker)
[![usage as download](https://img.shields.io/npm/dy/quiz-maker.svg)](https://www.npmjs.com/package/quiz-maker)

[EN](README.md) | FR

# Quiz Maker

Générateur de quiz à partir d'un fichier text brut (type Markdown, yaml ou json).

- [x] **Simple et puissant** :
  - [x] Modifiez un [exemple](demo-examples/), glissez-le sur le [générateur en ligne](https://1000i100.frama.io/quiz-maker/) et téléchargez votre quiz prêt à l'emploi.
  - [ ] Éditez votre quiz, en ligne, à plusieurs simultanément, avec le rendu temps réel à côté. Merci [HedgeDoc](https://hedgedoc.org/) pour l'intégration de quiz-maker dans ses pads.
  - [ ] Intégrez simplement vos quiz sur votre site wordpress grace au [plugin dédié]
  - [ ] Si votre besoin est simple, c'est simple à faire avec quiz-maker.
    Sinon [dite le](https://framagit.org/1000i100/quiz-maker/-/issues), histoire que ça s'arrange.
  - [ ] Si votre besoin est complexe, quiz-maker regorge d'option et vous pouvez étendre ses possibilités à l'infini.

- [x] **Éthique par design** : Par défaut, rien n'est collecté par personne.
  - [ ] Vous pouvez configurer vos quiz, sondage ou autre pour collecter, si vos usagers l'autorisent, les résultats.
  - Vous pouvez configurer vos quiz pour garder certains éléments secrets :
    - [ ] quotation
    - [ ] corpus de questions
- [x] **Pérenne** : vos quiz sont en format text, lisible aussi longtemps que l'informatique existera.
  quiz-maker est libre, n'importe quel développeur peut le faire évoluer selon vos besoins.

- [x] **Automatisable** : quiz-maker est utilisable en ligne de commande.
  Vous pouvez générer vos quiz à la volé dans un flux complexe de production spécifique à vos besoins.

- [x] **Versionable** : combiné à git, vos quiz se retrouvent sauvegardé en plusieurs endroits (perénité toujours),
  mais aussi versionné pour pouvoir identifier à quel état du quiz correspondent de vieux résultat de quiz.
  En outre git vous permet de concevoir vos quiz à plusieurs et facilite la fusion du travail de chacun.

- [ ] **Fiable** : la qualité de quiz-maker est suivi de près, à chaque nouvelle version, grace à différentes métriques :
  - **couverture de test** : indique la part du code source effectivement testée lors de l'exécution des tests automatisés.
    Ces derniers vérifient que les comportements du programme sont conforme aux attentes.
    [![coverage report](https://framagit.org/1000i100/quiz-maker/badges/main/coverage.svg)](https://1000i100.frama.io/quiz-maker/coverage/)
  - heuristique de **maintenabilité** : Un logiciel peut être fonctionnel en l'état mais trop dur à comprendre et à modifier pour que quiconque le face évoluer.
    Il existe des [bonnes pratiques](https://damien.pobel.fr/post/clean-code/) pour éviter au mieux cela et garder le code en bonne santé.
    Certaines sont mesurable et permette d'identifier quels endroits ont besoin de soin.
    [![duplication](https://1000i100.frama.io/quiz-maker/jscpd-badge.svg)](https://1000i100.frama.io/quiz-maker/jscpd/)
    [![maintainability](https://1000i100.frama.io/quiz-maker/worst-maintainability.svg)](https://1000i100.frama.io/quiz-maker/maintainability/)

- [ ] **Compatibilité optimisée** : En respectant le [versionnage sémantique](https://semver.org/lang/fr/),
  Il n'y a que des bonnes surprises (ou presque) :
  - les correctifs n'altèrent pas l'existant (les tests automatiques sont là pour le vérifier)
  - chaque version mineure indique des nouveautés retro-compatible
  - chaque version majeure permet l'évolution du logiciel au-delà des contraintes qu'imposait la rétro-compatibilité.
    Le nécessaire pour migrer vers une nouvelle version majeure est publié,
    à minima via le [changelog](CHANGELOG.fr.md) et autant que possible sous forme de guide de migration
    et script automatisant les conversions nécessaires.

- [x] **Zero dépendances** : les quiz générés peuvent fonctionner hors ligne.
  Si vous les avez configuré pour collecter les résultats, ou pour garder certains éléments secrets, l'usage hors ligne nécessite un [serveur local](https://www.apachefriends.org/index.html).

- [x] **Générateur autonome** : quiz-maker fonctionne sans dépendances.
  Il ne réinvente cependant pas la roue. À l'assemblage, il internalise ses dépendances.

Légende :
- [x] Fait
- [ ] Prévu

## [Générateur en ligne](https://1000i100.frama.io/quiz-maker/)

## Exemple de Quiz
- [Quiz de démonstration](https://1000i100.frama.io/quiz-maker/demo-examples/quiz.html) et le [fichier source](https://framagit.org/1000i100/quiz-maker/raw/main/demo-examples/quiz.txt) dont est issue cette démo.
- [Quiz de démonstration : agression-fuite-inibition](https://1000i100.frama.io/quiz-maker/demo-examples/agression-fuite-inibition.html) et le [fichier source](https://framagit.org/1000i100/quiz-maker/-/blob/main/demo-examples/agression-fuite-inibition.quiz) dont est issue cette démo.

<iframe src="https://1000i100.frama.io/quiz-maker/demo-examples/" width="100%" height="500px"></iframe>

## Usage en ligne de commande :
```
npx quiz-maker srcFileOrFolder targetFolder
```
